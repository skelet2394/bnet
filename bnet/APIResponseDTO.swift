//
//  APIResponseDTO.swift
//  bnet
//
//  Created by Valery Silin on 28/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

struct APIResponseDTO<T:Decodable>
{
    let status: Int
    let data: T
}

extension APIResponseDTO: Decodable
{
    private enum CodingKeys: String, CodingKey
    {
        case status, data
    }
    init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        status = try container.decode(Int.self, forKey: .status)
        data = try container.decode(T.self, forKey: .data)
    }
}

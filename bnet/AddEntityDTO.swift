//
//  AddEntityDTO.swift
//  bnet
//
//  Created by Valery Silin on 28/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

struct AddedEntryDTO: Decodable
{
    let id: String
}

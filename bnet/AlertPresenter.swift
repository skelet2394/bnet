//
//  AlertPresenter.swift
//  bnet
//
//  Created by Valery Silin on 29/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import UIKit

class AlertPresenter
{
    func presentAlert(title: String = "Error", message: String, viewController: UIViewController?, completion: (() -> ())?)
    {
        guard let vc = viewController else { return }
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Update data",
                                      style: .default,
                                      handler:
            { [weak self] (action) in
                completion?()
                alert.dismiss(animated: true, completion: nil)
        }))
        vc.present(alert, animated: true)
    }
}

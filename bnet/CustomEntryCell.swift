//
//  CustomEntryCell.swift
//  bnet
//
//  Created by Valery Silin on 29/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import UIKit

class CustomEntryCell: UITableViewCell
{
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var dateAddedLabel: UILabel!
    @IBOutlet weak var dateModifiedLabel: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    func configure(entry: EntryDTO)
    {
        bodyLabel.text = entry.body.truncate(length: 200)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm dd-MM-YYYY"
        if entry.dateAdded == entry.dateModified
        {
            dateModifiedLabel.isHidden = true
        }
        else
        {
            let modified = dateFormatter.string(from: entry.dateModified)
            dateModifiedLabel.text = "Modified: \(modified)"
        }
        let added = dateFormatter.string(from: entry.dateAdded)
        dateAddedLabel.text = "Added: \(added)"
    }
}

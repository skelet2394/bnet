//
//  Date + Extensions.swift
//  bnet
//
//  Created by Valery Silin on 29/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//
import Foundation

extension Date
{
    static func formattedString(string: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz" // 
        
        let formateDate = dateFormatter.date(from:"2018-02-02 06:50:16 +0000")!
        dateFormatter.dateFormat = "dd-MM-yyyy" // Output Formated
        
        print ("Print :\(dateFormatter.string(from: formateDate))")//Print :02-02-2018
        return dateFormatter.string(from: formateDate)
    }
}


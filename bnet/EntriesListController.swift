//
//  EntriesListController.swift
//  bnet
//
//  Created by Valery Silin on 27/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import UIKit

class EntriesListController: UIViewController
{
    
    var entriesArray = [EntryDTO]()
    
    @IBOutlet weak var entriesTableView: UITableView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        entriesTableView.register(UINib(nibName: "CustomEntryCell", bundle: nil), forCellReuseIdentifier: "entryCell")
    }
    override func viewWillAppear(_ animated: Bool)
    {
        getEntries()
    }
    
    @IBAction func addEntryPressed(_ sender: Any)
    {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "EntryDetailController") as? EntryDetailController
        {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func updateEntriesPressed(_ sender: Any)
    {
        getEntries()
    }
    func getEntries()
    {
        SessionService().getSession { [weak self] (result) in
            switch result
            {
            case .success(let session):
                NetworkService().getEntries(session: session, completion:
                    { (result) in
                        switch result
                        {
                        case .success(let entries):
                            let flattenedEntries = entries.flatMap {$0}
                            self?.entriesArray.removeAll()
                            self?.entriesArray.append(contentsOf: flattenedEntries)
                            self?.entriesTableView.reloadData()
                        case .failure(let error):
                            AlertPresenter().presentAlert(message: error.localizedDescription, viewController: self ?? nil, completion: { self?.getEntries() })
                        }
                })
            case .failure(let error):
                AlertPresenter().presentAlert(message: error.localizedDescription, viewController: self ?? nil, completion: { self?.getEntries() })
            }
        }
    }
}

extension EntriesListController: UITableViewDelegate
{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        if let vc = storyboard?.instantiateViewController(withIdentifier: "EntryDetailController") as? EntryDetailController
        {
            vc.selectedEntry = entriesArray[indexPath.row]
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension EntriesListController: UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return entriesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        guard let cell = entriesTableView.dequeueReusableCell(withIdentifier: "entryCell", for: indexPath) as? CustomEntryCell else
        {
            let cell = UITableViewCell(style: .default, reuseIdentifier: "entryCell")
            return cell
        }
        cell.configure(entry: entriesArray[indexPath.row])
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int
    {
        var numOfSections = 0
        if entriesArray.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "No data available"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
}

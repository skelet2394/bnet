//
//  EntryDTO.swift
//  bnet
//
//  Created by Valery Silin on 28/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//
import Foundation
struct EntryDTO: Decodable
{
    let id: String
    let body: String
    let dateAdded: Date
    let dateModified: Date
    
    enum CodingKeys: String, CodingKey
    {
        case id, body
        case dateAdded = "da"
        case dateModified = "dm"
    }
}

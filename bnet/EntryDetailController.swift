//
//  EntryDetailController.swift
//  bnet
//
//  Created by Valery Silin on 29/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import UIKit

class EntryDetailController: UIViewController
{
    var selectedEntry: EntryDTO!
    
    @IBOutlet weak var entryTextView: UITextView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        selectedEntry == nil ? configureForEditing() : configureWithEntry()
    }
    
    func configureWithEntry()
    {
        entryTextView.text = self.selectedEntry.body
        entryTextView.isEditable = false
    }
    func configureForEditing()
    {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save,
                                                            target: self,
                                                            action: #selector(addEntry))
    }
    @objc func addEntry()
    {
        SessionService().getSession { [weak self] (result) in
            switch result
            {
            case .success(let session):
                NetworkService().addEntry(session: session, body: self?.entryTextView.text ?? "", completion:
                    {result in
                    switch result
                    {
                    case .success(_):
                        self?.navigationController?.popViewController(animated: true)
                    case .failure(let error):
                        AlertPresenter().presentAlert(message: error.localizedDescription, viewController: self ?? nil, completion: { self?.addEntry() })
                        }
                })
            case .failure(let error):
                print(error)
            }
        }
    }
}

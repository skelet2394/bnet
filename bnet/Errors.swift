//
//  Errors.swift
//  bnet
//
//  Created by Valery Silin on 29/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Foundation
enum APIError: Error
{
    case networkError
    case dataError
    case custom(String)
}
extension APIError: LocalizedError
{
    var errorDescription: String?
    {
        switch self
        {
        case .networkError:
            return "Check your network connection and try again"
        case .dataError:
            return "Data error"
        case .custom (let message):
            return message
        }
    }
}

enum Result<Data>
{
    case success(Data)
    case failure(APIError)
}

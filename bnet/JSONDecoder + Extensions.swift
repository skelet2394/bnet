//
//  JSONDecoder + Extensions.swift
//  bnet
//
//  Created by Valery Silin on 28/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//
import Foundation
public final class DefaultJSONDecoder: JSONDecoder
{
    override public init()
    {
        super.init()
        self.dateDecodingStrategy = .custom(
            { (decoder) -> Date in
                let data = try decoder.singleValueContainer()
                let string = try data.decode(String.self)
                guard let double = Double(string) else
                {
                    throw DecodingError.dataCorruptedError(in: data,
                                                           debugDescription: "Unable to convert string to double")
                }
                guard let unixTime = TimeInterval(exactly: double) else
                {
                    throw DecodingError.dataCorruptedError(in: data,
                                                           debugDescription: "Unable to convert double to unixtimestamp")
                }
                return Date(timeIntervalSince1970: unixTime)
        })
    }
}

//extension Formatter
//{
//    static let iso8601: DateFormatter = {
//        let formatter = DateFormatter()
//        formatter.calendar = Calendar(identifier: .iso8601)
//        formatter.locale = Locale(identifier: "en_US_POSIX")
//        formatter.timeZone = TimeZone(secondsFromGMT: 0)
//        formatter.dateУ
//    }
//}

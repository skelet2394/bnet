//
//  NetworkRouter.swift
//  bnet
//
//  Created by Valery Silin on 27/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Moya

enum NetworkRouter
{
    case newSession
    case getEntries(session: String)
    case addEntry(session: String, body: String)
}
extension NetworkRouter: TargetType
{
    var baseURL: URL
    {
        return URL(string: "https://bnet.i-partner.ru")!
    }
    
    var path: String
    {
        return "/testAPI/"
    }
    
    var method: Method
    {
        return .post
    }
    
    var sampleData: Data
    {
        return "".data(using: .utf8)!
    }
    
    var task: Task
    {
        switch self
        {
        case .newSession:
            return .requestParameters(parameters: ["a": "new_session"],
                                      encoding: URLEncoding.httpBody)
            
        case .getEntries(let session):
            return .requestParameters(parameters: ["a": "get_entries",
                                                   "session": session],
                                      encoding: URLEncoding.httpBody)

        case .addEntry(let session, let body):
            return .requestParameters(parameters: ["a": "add_entry",
                                                   "session" : session,
                                                   "body" : body],
                                      encoding: URLEncoding.httpBody)
        }
    }
    
    var headers: [String : String]?
    {
        return ["token" : GlobalParameters().token]
    }
    
    
}

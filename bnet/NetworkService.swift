//
//  NetworkService.swift
//  bnet
//
//  Created by Valery Silin on 27/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Moya

class NetworkService
{
    private let provider = MoyaProvider<NetworkRouter>(
//        plugins: [NetworkLoggerPlugin(verbose: true)]
    )
    
    private func networkRequest<T>(endpoint: NetworkRouter,
                                   completion: @escaping (Result<T>) -> ())
        where T: Decodable
    {
        provider.request(endpoint)
        { (result) in
            switch result
            {
            case .success(let moyaResponse):
                do
                {
                    let filteredResponse = try moyaResponse.filterSuccessfulStatusCodes()
                    let data = try filteredResponse.map(APIResponseDTO<T>.self,
                                                        using: DefaultJSONDecoder(),
                                                        failsOnEmptyData: false)
                    completion(.success(data.data))
                }
                catch
                {
                    completion(.failure(APIError.dataError))
                }
            case .failure(let error):
                completion(.failure(APIError.networkError))
            }
        }
    }
    
    func newSession(token: String, completion: @escaping (Result<SessionDTO>) -> ())
    {
        networkRequest(endpoint: NetworkRouter.newSession)
        { (result: Result<SessionDTO>) in
            
            completion(result)
        }
    }
    func getEntries(session: String, completion: @escaping (Result<[[EntryDTO]]>) -> ())
    {
        networkRequest(endpoint: NetworkRouter.getEntries(session: session))
        { (result: Result<[[EntryDTO]]>) in
            completion(result)
        }
    }
    func addEntry(session: String, body: String, completion: @escaping (Result<AddedEntryDTO>) -> ())
    {
        networkRequest(endpoint: NetworkRouter.addEntry(session: session, body: body))
        { (result: Result<AddedEntryDTO>) in
            completion(result)
        }
    }
    
}

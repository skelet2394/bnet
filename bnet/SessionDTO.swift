//
//  SessionDTO.swift
//  bnet
//
//  Created by Valery Silin on 28/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

struct SessionDTO: Decodable
{
    let session: String
}

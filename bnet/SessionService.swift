//
//  SessionService.swift
//  bnet
//
//  Created by Valery Silin on 29/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

import Foundation

class SessionService
{
    func getSession(completion: @escaping (Result<String>) -> ())
    {
        if let localSession = UserDefaults.standard.string(forKey: "session")
        {
            completion(.success(localSession))
        }
        else
        {
            NetworkService().newSession(token: GlobalParameters().token)
            { [weak self] (object) in
                switch object
                {
                case .success(let object):
                    UserDefaults.standard.set(object.session, forKey: "session")
                    completion(.success(object.session))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
}

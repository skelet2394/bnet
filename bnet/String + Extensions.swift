//
//  String + Extensions.swift
//  bnet
//
//  Created by Valery Silin on 29/01/2019.
//  Copyright © 2019 Valery Silin. All rights reserved.
//

extension String
{
    func truncate(length: Int, trailing: String = "…") -> String
    {
        return (self.count > length) ? self.prefix(length) + trailing : self
    }
}

